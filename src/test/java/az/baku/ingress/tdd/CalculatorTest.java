package az.baku.ingress.tdd;


import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.*;

public class CalculatorTest {

    Calculator calculator = new Calculator();

    @Test
    void givenAAndBWhenAddThenC() {

        int a = 4;
        int b = 3;

        int c = calculator.add(a, b);
        assertThat(c).isEqualTo(a + b);


    }

    @Test
    void givenAAndBWhenSubtractThenC() {
        int a = 10;
        int b = 6;

        int c = calculator.subtract(a, b);
        assertThat(c).isEqualTo(a - b);
    }


    @Test
    void givenAAndBWhenDivideThenC() {

        int a = 15;
        int b = 3;

        int c = calculator.divide(a, b);

        assertThat(c).isEqualTo(a / b);
    }

    @Test
    void givenAAndBWhenDivideByZeroThenException() {

        int a = 12;
        int b = 0;

        assertThatThrownBy(() -> calculator.divide(a, b))
                .isInstanceOf(ArithmeticException.class);
    }
}
