package az.baku.ingress.tdd;

public class Calculator {

    public int add(int a, int b) {
        return addPrivate(a, b);
    }


    public int subtract(int a, int b) {

        return a - b;
    }

    public int divide(int a, int b) {
        return a / b;
    }

    private int addPrivate(int a, int b) {
        return a + b;
    }
}
